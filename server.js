// launch from cygwin command:
// node "c:\Users\simon\Documents\git\cedreload\server.js" --sync-to ~/Sites/wordpress/wp/wp-content/themes/azet-theme
// (yep, both styles of patch, windows style for node parameter, and cygwin style for rsync parameter...)

// file watcher
const fs = require('fs');
const { exec } = require('child_process'); // only interested in exec from that module

// common settings
const throttle = 500; // time to wait between browser refresh...

//express.js related
const PORT = 8088;
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);

// serving static files
app.use(express.static(__dirname + '/public'));

// watching source folder
const watch_folder = process.cwd();
console.info( "Watching changes in ", watch_folder);

// vars for my app
var ready_last = new Date();
var useRsync = false;
var rsyncTarget = null;
var rsyncRunning = false;
const fsWatchIgnore = ['.git', '.svn', 'logs', 'templates_c']; // what folders we skip when whatching content change

//======================================================================
// init the headers for the browser
//======================================================================

// middle-ware for express to allow requests from any origin
app.use(function(req, res, next) {
	// source: https://enable-cors.org/server_expressjs.html
	res.header('Access-Control-Allow-Origin', '*');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// REST API check entry from the browser
//======================================================================
app.get('/check', function(req, res) {
	// new protocol for numerous clients
	res.write(JSON.stringify(ready_last));
	res.end();
});

// SOCKET IO (websocket)
// =====================
io.on('connection', function(client) {
	console.log("Client connected...");

	client.on('disconnect', function(data_) {
		console.log("Client disconnected...");
	});
});

//======================================================================

// if --sync-to argument is passed with a path, we sync files we are watching to a destination with rsync
function startRsync() {
	// only one copy at a time
	if(rsyncRunning) return;
	
	console.log("starting rsync...");
	rsyncRunning = true;
	// var rsyncCommand = "rsync -av --dry-run ./ " + rsyncTarget; // DBG dry run
	var rsyncCommand = "rsync -av ./ " + rsyncTarget; // the real thing
	// console.log("command:", rsyncCommand);
	exec(rsyncCommand, (error, stdout, stderr) => {
		// rsync command is over
		rsyncRunning = false;
		console.log("rsync done.");
	});
}

// will send a signal to connected browsers
// once a trigger condition is met
function informClients () {
	let now = new Date();

	// is it too quick?
	let diff = now.getTime() - ready_last.getTime();

	// no need to notify the browsers
	if(diff < throttle) return;

	// save current timestamp for next call
	ready_last = now;

	// launch rsync to copy files to a mirror location
	if(useRsync) startRsync();

	// we notify the browsers
	io.emit('reload', 'ok');
}


//======================================================================
// listening for changes in folder the command is run in
//======================================================================


/**
 * Function that checks if a filename (with its path) contains 
 * a substring that we want to skip.
 * The goal is to not send a reload signal to browser
 * when source control files are updated.
 * 
 * @param  {string} file_name_ the filename path we want to check 
 * @return {bool} filename is to be ignored or not
 */
function ignoreFileChange(file_name_) {
	for(var i in fsWatchIgnore) {
		var elem = fsWatchIgnore[i];
		if(file_name_.indexOf(elem) !== -1) {
			return true;
		}
	}

	return false;
}


fs.watch(watch_folder, {recursive: true}, (event_, file_) => {
	// skip files we don't want to inform clients when changed
	if(file_ && ignoreFileChange(file_)) return;

	console.log(event_, file_); // this is called twice...
	// let clients know...
	informClients();
});

//======================================================================
// petit message when starting
//======================================================================

server.listen(PORT);
console.info('Server running on port '+PORT);

// new socket API
console.info('Add <script src="http://192.168.1.201:'+PORT+'/socket.io/socket.io.js"></script> in your page.');
console.info('Add <script src="http://192.168.1.201:'+PORT+'/cedreload.io.js"></script> in your page.');


// checking parameters and see if we want to duplicate the folder
if( process.argv[2] == '--sync-to' && process.argv[3] ) {
	useRsync = true;
	rsyncTarget = process.argv[3];
	console.log( "activating rsync into folder " + rsyncTarget );
	console.info("ONLY WORKS IN CYGWIN terminal, NOT TO USE IN COMMAND PROMPT!");
}

//======================================================================
// Ending the server
//======================================================================

process.on('SIGINT', function() {
	console.log('Recieve SIGINT');
	process.exit();
});
