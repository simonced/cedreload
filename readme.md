
# Environment

For now, it's a system that is used under Windows.  
NodeJS installed in Windows.  
Cygwin installed for rsync. _(but it needs to be run from cygwin, error happens when run from command prompt)_


# Launching

```
node "c:\cedreload\server.js" --sync-to ~/destination/folder
```

# TODO

- Send signal to browser with Websocket.
- Make the syncing work from command prompt (even if using cygwin rsync binary, or use a different rsync for Windows)
